fletapp.controller('openLayerCtrl',
        ['$scope', 'Api', function ($scope, Api)
            {
                // INICIA EL CARGADO DE MAPAS!  
                var wmsSource = new ol.source.ImageWMS({
                    url: 'http://localhost:8081/geoserver/TSIG/wms',
                    params: {'layers': 'TSIG:TSIG', 'bbox': '551796.375,6133367.0,589413.5,6159942.0'},
                    serverType: 'geoserver',
                    crossOrigin: 'anonymous'
                });

                var wmsLayer = new ol.layer.Image({
                    source: wmsSource
                });

                var vectorLayer = new ol.layer.Vector({
                    source: new ol.source.Vector({
                        format: new ol.format.GeoJSON(),
                        url: 'http://localhost:8081/geoserver/TSIG/ows?service=WFS&version=1.1.0' +
                                '&request=GetFeature&typeName=TSIG:fletes&featureID=fletes.24,fletes.25&srsname=EPSG:32721' +
                                '&outputFormat=application%2Fjson'
                    }),
                    style: new ol.style.Style({
                        fill: new ol.style.Fill({
                            color: 'rgba(66, 244, 176, 0.3)'
                        }),
                        stroke: new ol.style.Stroke({
                            color: '#42f4b0',
                            width: 2
                        }),
                        image: new ol.style.Circle({
                            radius: 7,
                            fill: new ol.style.Fill({
                                color: '#42f4b0'
                            })
                        })
                    })
                });

                var projection = new ol.proj.Projection({
                    code: 'EPSG:32721',
                    units: 'm'
                });

                var map = new ol.Map({
                    layers: [wmsLayer],
                    target: 'map',
                    view: new ol.View({
                        center: [578471.5167414253, 6137787.661694823],
                        projection: projection,
                        zoom: 13
                    })
                });
                var zonas = [1, 2, 3, 4, 5]
                for (i = 0; i < zonas.length; i++) {

                    var zonasPosi = new ol.layer.Vector({
                        source: new ol.source.Vector({
                            format: new ol.format.GeoJSON(),
                            url: 'http://localhost:8081/geoserver/TSIG/ows?' +
                                    'service=WFS&version=1.0.0&request=GetFeature&typeName=TSIG:sugerirFletes&srsname=EPSG:32721'
                                    + '&outputFormat=application%2Fjson&viewparams=zonaPosible:' + zonas[i]
                        }),
                        style: new ol.style.Style({
                            image: new ol.style.Circle({
                                radius: 7,
                                fill: new ol.style.Fill({
                                    color: '#42f4b0'
                                })
                            })
                        })
                    });
                    map.addLayer(zonasPosi);
                }

                // DIBUJEMOS UN RATO
                var features = new ol.Collection();
                var featureOverlay = new ol.layer.Vector({
                    source: new ol.source.Vector({features: features}),
                    style: new ol.style.Style({
                        fill: new ol.style.Fill({
                            color: 'rgba(66, 244, 176, 0.3)'
                        }),
                        stroke: new ol.style.Stroke({
                            color: '#42f4b0',
                            width: 2
                        }),
                        image: new ol.style.Circle({
                            radius: 7,
                            fill: new ol.style.Fill({
                                color: '#42f4b0'
                            })
                        })
                    })
                });
                featureOverlay.setMap(map);

                var feature = null;
                $scope.funcionInicio = function () {
                    draw = new ol.interaction.Draw({
                        features: features,
                        type: 'Polygon',
                        style: new ol.style.Style({
                            fill: new ol.style.Fill({
                                color: 'rgba(66, 244, 176, 0.3)'
                            }),
                            stroke: new ol.style.Stroke({
                                color: '#42f4b0',
                                width: 2
                            }),
                            image: new ol.style.Circle({
                                radius: 7,
                                fill: new ol.style.Fill({
                                    color: '#42f4b0'
                                })
                            })
                        })
                    });
                    map.addInteraction(draw);
                    draw.on('drawend', function (evt) {
                        feature = evt.feature;
                        map.removeInteraction(draw);
                    });
                };

                // MODIFICANDO LO DIBUJADO!
                var modify = new ol.interaction.Modify({
                    features: features
                });
                map.addInteraction(modify);

                //    ALMACENEMOS LOS DATOS!


                var formatGML = new ol.format.GML({
                    featureNS: 'TSIG',
                    featureType: 'zonas'
                });

                var formatWFS = new ol.format.WFS();

                var xs = new XMLSerializer();

                var id = null;
                $scope.funcionGuardar = function () {
                    var node = formatWFS.writeTransaction([feature], null, null, formatGML);
                    var payload = xs.serializeToString(node);
                    $.ajax('http://localhost:8081/geoserver/wfs', {
                        service: 'WFS',
                        type: 'POST',
                        dataType: 'xml',
                        processData: false,
                        contentType: 'text/xml',
                        data: payload
                    }).done(function (resp) {
                        var result = formatWFS.readTransactionResponse(resp);
                        id = result.insertIds[0];
                        console.log("result:", id);
                    });
                };
            }
        ]);