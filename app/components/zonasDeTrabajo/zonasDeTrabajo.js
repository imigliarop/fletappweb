fletapp.controller('zonasDeTrabajoCtrl', ['$scope', '$route', 'Api','geoService', 'Session', '$timeout',
    function($scope, $route, api, geoservice, session, $timeout){
        
        $scope.opcionesMapa = {
            tipo: 'zonasDeTrabajo',
            zonaSeleccionada: null
        };
        
        $scope.geoZonaDeTrabajo = null;
        
        $scope.zonaDeTrabajo = {
            id: null,
            nombre: '',
            precio: 0.0
        };
        
        $scope.obtenerZonasDeTrabajo = function(){
           var fleteroId = {"fleteroId": session.get('usuario').id};
           api.get('usuario/zonasPorFletero', fleteroId, $scope.cargarZonasDeTrabajo);
        };
        
        $scope.cargarZonasDeTrabajo = function(resp){
            $scope.zonasDeTrabajo = resp.data;
        };
        
        $scope.seleccionarZona = function(zona){
            $scope.zonaDeTrabajo = zona;
            $scope.opcionesMapa.zonaSeleccionada = zona.id;
            $scope.$apply();
        };
        
        $scope.nuevaZona = function(){
            $scope.zonaDeTrabajo = {
                id: null,
                nombre: '',
                precio: 0.0
            };
            $scope.opcionesMapa.zonaSeleccionada = null;
            $scope.zonaForm.$setPristine();
        };
        
        $scope.guardar = function(){
            $scope.zonaForm.$setSubmitted();
            if ($scope.zonaForm.$invalid) {
                return;
            }
            
            var payloadZona = $scope.geoZonaDeTrabajo;
            if (payloadZona !== null){
                geoservice.geoPost(payloadZona).then(function(res){
                    $scope.zonaDeTrabajo.id = geoservice.formatXml(res.data);
                    var usuarioId = session.get("usuario").id;                
                    var urlData = 'usuario/zonaDeTrabajo';
                    api.post(urlData, $scope.zonaDeTrabajo, limpiarFormulario, {"fleteroId": usuarioId});
                });
            }
        };
        
        var limpiarFormulario = function (res) {
            $scope.mensaje = "La zona se creó correctamente.";
            $('#successModal').modal('show');
            $timeout(function () {
                $('#successModal').modal('hide');
                $timeout(function () {
                    $route.reload();
                }, 1000);
            }, 2500);
        }; 
        
        $scope.obtenerZonasDeTrabajo();
    }
]);