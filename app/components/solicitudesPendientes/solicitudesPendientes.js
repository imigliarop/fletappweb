/*******************************************************************************
 * la idea es obtener todas las solicitudes pendientes para las fechas indicadas 
 * que cumplan con los maximos que permite le vehiculo del fletero que consulta. 
 * Una vez obtenidas, se listan todas, y se da la opcion de filtrar por zona de 
 * trabajo. Solo se filtra una zona, se hace una consulta a geoserver por: zona 
 * de trabajo. En el momento de aceptar la solicitud corroborar que siga pendiente.
 ********************************************************************************/
fletapp.controller('solicitudesPendientesCtrl',
        ['$scope', '$route', 'Api', 'geoService', 'Session', '$timeout', '$filter', '$q',
            function ($scope, $route, api, geoservice, session, $timeout, $filter, $q) {

                $scope.fechaConfig = {
                    startView: 'day',
                    minView: 'day',
                    dropdownSelector: '#dropdown2'
                };

                $scope.fechaDesde = null;
                $scope.fechaHasta = null;
                $scope.geoPuntos = null;

                $scope.opcionesMapa = {
                    tipo: 'solicitudesPendientes',
                    inicio: null,
                    fin: null
                };

                $scope.zonaDeTrabajo = null;

//                $scope.solicitud = {
//                    clienteId: null,
//                    nombre: '',
//                    apellido: '',
//                    fechaHora: null,
//                    peso: '',
//                    volumen: '',
//                    precioSolicitado: '',
//                    puntoOrigen: null,
//                    puntoDestino: null
//                };

                $scope.obtenerZonasDeTrabajo = function () {
                    var fleteroId = {"fleteroId": session.get('usuario').id};
                    api.get('usuario/zonasPorFletero', fleteroId, $scope.cargarZonasDeTrabajo);
                };

                $scope.cargarZonasDeTrabajo = function (resp) {
                    $scope.zonasDeTrabajo = resp.data;
                    for (var i = 0; i < $scope.zonasDeTrabajo.length; i++) {
                        $scope.zonasDeTrabajo[i].solicitudes = [];
                    }
                };

                $scope.obtenerSolicitudesPendientes = function () {
                    var consulta = {"usuarioId": session.get('usuario').id,
                        "fechaDesde": $scope.fechaDesde,
                        "fechaHasta": $scope.fechaHasta};
                    api.post('flete/obtenerSolicitudesPendientes', consulta, $scope.cargarSolicitudesPendientes);
                };

                $scope.cargarSolicitudesPendientes = function (resp) {
                    $scope.solicitudesPendientes = [];
                    for (var i = 0; i < $scope.zonasDeTrabajo.length; i++) {
                        $scope.zonasDeTrabajo[i].solicitudes = [];
                    }
                    $scope.solicitudesExistentes = resp.data;
                    var zonas = $filter('orderBy')($scope.zonasDeTrabajo, 'precio');
                    $scope.filtrarSolicitudesPorZona(zonas);
                };

                $scope.filtrarSolicitudesPorZona = function (zonas) {
                    if (zonas.length === 0) {
                        actualizarSolicitudesPendientes();
                        return;
                    }

                    //Cargo las promesas de consulta a geoserver en el mismo orden que estan las solicitudes pendientes
                    //por lo tanto el indice se correlaciona
                    var promises = [];

                    //limpio las solicitudes de la zona
                    //$filter('filter')($scope.zonasDeTrabajo, {id: zonas[0].id})[0].solicitudes - [];

                    for (var i = 0; i < $scope.solicitudesExistentes.length; i++) {
                        var origen = $scope.solicitudesExistentes[i].puntoOrigen;
                        var destino = $scope.solicitudesExistentes[i].puntoDestino;
                        promises.push(geoservice.getDistanciaDeSolicitud(zonas[0].id, origen, destino));
                    }

                    $q.all(promises).then((responses) => {
                        //Recorro la lista de promesas al reves por si tengo que eliminar solicitudes
                        for (var i = responses.length - 1; i >= 0; i--) {
                            if (responses[i].data.features.length > 0) {
                                //obtengo la distancia del flete y calculo su costo
                                //en funcion del precio de la zona
                                var miPrecio = responses[i].data.features[0].properties.distancia / 1000 * zonas[0].precio;
                                if ($scope.solicitudesExistentes[i].precioSolicitado >= miPrecio) {
                                    //$scope.solicitudesPendientes.push($scope.solicitudesExistentes[i]);
                                    $filter('filter')($scope.zonasDeTrabajo, {id: zonas[0].id})[0].solicitudes.push($scope.solicitudesExistentes[i]);
                                    //elimino la solicitud asi las proximas zonas no comparan
                                    $scope.solicitudesExistentes.splice(i, 1);
                                }
                            }
                        }
                        zonas.shift();
                        $scope.filtrarSolicitudesPorZona(zonas);
                    });
                };

                $scope.aceptarFlete = function (solicitud) {
                    console.log(solicitud);
                };

                $scope.seleccionarZona = function (zona) {
                    $scope.zonaDeTrabajo = zona;
                };

                $scope.obtenerZonasDeTrabajo();

                $scope.$watch('zonaDeTrabajo', function () {
                    actualizarSolicitudesPendientes();
                });

                var actualizarSolicitudesPendientes = function () {
                    if ($scope.zonaDeTrabajo != null) {
                        $scope.solicitudesPendientes = $scope.zonaDeTrabajo.solicitudes;
                    } else {
                        if ($scope.zonasDeTrabajo) {
                            $scope.solicitudesPendientes = [];
                            for (var i = 0; i < $scope.zonasDeTrabajo.length; i++) {
                                $scope.solicitudesPendientes = $scope.solicitudesPendientes.concat($scope.zonasDeTrabajo[i].solicitudes);
                            }
                        }
                    }
                };
            }
        ]);

