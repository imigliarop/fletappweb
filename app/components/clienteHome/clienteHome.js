fletapp.controller('clienteHomeCtrl', ['$rootScope', '$scope', 'Session', '$location', 'Api', '$timeout', '$route', '$q', 'geoService',
    function ($rootScope, $scope, session, $location, api, $timeout, $route, $q, geoService) {

        //Si hay un usuario logueado, y hay un flete en el rootScope, cargarlo e ir directamente a la solicitud
        $scope.usuario = session.get("usuario");

        $scope.opcionesMapa = {
            tipo: 'solicitudFlete'
        };

        if ($scope.usuario && $rootScope.flete) {
            $scope.flete = $rootScope.flete;
            $scope.opcionesMapa.ptoInicio = $scope.flete.puntoOrigen;
            $scope.opcionesMapa.ptoFin = $scope.flete.puntoDestino;
            $scope.opcionesMapa.fleteSeleccionado = 'true';


        } else {
            $scope.geoPuntos = null;
            $scope.flete = {
                fechaHora: "",
                precioSolicitado: "",
                peso: "",
                volumen: "",
                clienteId: ""
            };
            $scope.opcionesMapa.cargarSeleccion = null;
        }

        $scope.fechaConfig = {
            startView: 'day',
            dropdownSelector: '#dropdown2'
        };

        $scope.solicitarFlete = function (tipoAcceso) {
            //Valido el formulario
            if ($scope.fleteForm.$invalid) {
                $scope.fleteForm.fechaHora.$dirty = true;
                $scope.fleteForm.$dirty = true;
                return;
            }

            //Checkear que los dos puntos esten guardados
            if ($scope.flete.puntoOrigen == null || $scope.flete.puntoDestino == null) {
                return;
            }

            //Si el usuario esta logueado guardo los puntos y luego el flete
            if (session.get("usuario")) {
                $scope.flete.clienteId = session.get("usuario").id;
                api.post('flete/solicitar', $scope.flete, limpiarFormulario);
            } else {
                $rootScope.flete = $scope.flete;
                $rootScope.geoPuntos = $scope.geoPuntos;
                $location.path('/' + tipoAcceso + '/Cliente');
            }
        };

        var limpiarFormulario = function (res) {
            $rootScope.flete = null;
            $scope.mensaje = res.data.mensaje;
            $('#successModal').modal('show');
            $timeout(function () {
                $('#successModal').modal('hide');
                $timeout(function () {
                    $route.reload();
                }, 1000);
            }, 2500);
        };

        $scope.$watchCollection('geoPuntos', function (newValue) {
            if (!newValue) {
                return;
            }
            var nuevoPuntoOrigen = null;
            var grabarPuntoOrigen = false;
            var nuevoPuntoDestino = null;
            var grabarPuntoDestino = false;

            //Cargar las variables del scope para poder comparar
            //Cargar los nuevos puntos de origen y destino
            if ($scope.puntoOrigen == undefined || $scope.puntoOrigen == null) {
                $scope.puntoOrigen = newValue.find(g => g.punto === 'origen');
                grabarPuntoOrigen = true;
            } else {
                nuevoPuntoOrigen = newValue.find(g => g.punto === 'origen');
                //Revisar si cambió el punto de origen
                if (nuevoPuntoOrigen !== null && nuevoPuntoOrigen !== $scope.puntoOrigen) {
                    $scope.puntoOrigen = nuevoPuntoOrigen;
                    grabarPuntoOrigen = true;
                }
            }
            if ($scope.puntoDestino == undefined || $scope.puntoDestino == null) {
                $scope.puntoDestino = newValue.find(g => g.punto === 'destino');
                grabarPuntoDestino = true;
            } else {
                nuevoPuntoDestino = newValue.find(g => g.punto === 'destino');
                //Revisar si cambió el punto de destino
                if (nuevoPuntoDestino !== null && nuevoPuntoDestino !== $scope.puntoDestino) {
                    $scope.puntoDestino = nuevoPuntoDestino;
                    grabarPuntoDestino = true;
                }
            }

            //Grabar el punto de origen
            if ($scope.puntoOrigen != null && grabarPuntoOrigen) {
                var payloadOrigen = $scope.puntoOrigen.payload;
                geoService.geoPost(payloadOrigen).then(function (res) {
                    $scope.flete.puntoOrigen = geoService.formatXml(res.data);
                    //$scope.obtenerIdZonas();
                    $scope.obtenerDatosGeograficos();
                });
            }

            //Grabar el punto de Destino
            if ($scope.puntoDestino != null && grabarPuntoDestino) {
                var payloadDestino = $scope.puntoDestino.payload;
                geoService.geoPost(payloadDestino).then(function (res) {
                    $scope.flete.puntoDestino = geoService.formatXml(res.data);
                    //$scope.obtenerIdZonas();
                    $scope.obtenerDatosGeograficos();
                });
            }
        });

        $scope.obtenerDatosGeograficos = function () {
            if ($scope.flete.puntoOrigen != null && $scope.flete.puntoDestino != null) {
                var promises = {
                    zonas: geoService.getZonasParaSolicitud($scope.flete.puntoOrigen, $scope.flete.puntoDestino),
                    distancia: geoService.getDistancia($scope.flete.puntoOrigen, $scope.flete.puntoDestino)
                };

                $q.all(promises).then((values) => {
                    var zonas = values.zonas.data.features;
                    var zonasIds = [];
                    for (var i = 0; i < zonas.length; i++) {
                        zonasIds.push('zonas.' + zonas[i].properties.gid);
                    }

                    if (zonasIds.length > 0) {
                        var distancia = values.distancia.data.features[0].properties.distancia;
                        consultarSugerenciaDeFleteros(zonasIds, distancia);
                    } else{
                        $scope.fleteros = [];
                    }

                });
            }
        };

        var consultarSugerenciaDeFleteros = function (zonasIds, distancia) {
            var parametros = {
                "zonasIds": zonasIds,
                "volumen": $scope.flete.volumen,
                "peso": $scope.flete.peso,
                "distancia": distancia
            };

            api.get('usuario/sugerirFleterosParaFlete', parametros, cargarFleteros);
        };

        var cargarFleteros = function (res) {
            $scope.fleteros = res.data;
        };

    }
]);