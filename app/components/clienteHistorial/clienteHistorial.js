fletapp.controller('clienteHistorialCtrl', ['$rootScope', '$scope', 'Session', '$location', 'Api', '$timeout', '$route', '$q', 'geoService', '$http',
    function ($rootScope, $scope, session, $location, api, $timeout, $route, $q, geoService, $http) {

        $scope.opcionesMapa = {
            tipo: 'historialFlete',
            fleteSeleccionado: null,
            ptoInicio: null,
            ptoFin: null
        };

        $scope.fechaConfig = {
            startView: 'day',
            minView: 'day',
            dropdownSelector: '#dropdown2'
        };

        $scope.fechaDesde = null;
        $scope.fechaHasta = null;
        $scope.geoPuntos = null;

        $scope.historialFletes = {
            tipo: 'solicitudFlete'
        };

        $scope.listadoRealizados = null;
        $scope.listadoPendientes = null;

        $scope.seleccionarFlete = function (flete) {
            $scope.fleteSeleccionado = flete;
            $scope.opcionesMapa.ptoInicio = flete.puntoOrigen;
            $scope.opcionesMapa.ptoFin = flete.puntoDestino;
            $scope.opcionesMapa.fleteSeleccionado = 'true';
        };
        
        var puntajeGuardado = function(res){
            $scope.mensaje = "Calificación actualizada";
            $('#successModal').modal('show');
            $timeout(function () {
                $('#successModal').modal('hide');
            }, 2500);
        };

        $scope.puntuarFlete = function () {
            api.post('flete/puntuarFlete', {"id": $scope.fleteSeleccionado.id, "puntuacion": $scope.fleteSeleccionado.puntuacion}, puntajeGuardado);
        };

        $scope.obtenerFletesPendientes = function () {
            var idUser = {"usuarioId": session.get('usuario').id, "fechaDesde": $scope.fechaDesde, "fechaHasta": $scope.fechaHasta};
            api.post('flete/obtenerListaFletesPendientes', idUser, $scope.cargarListaFletesPendientes);
        };

        $scope.cargarListaFletesPendientes = function (resp) {
            $scope.listadoPendientes = resp.data;
        };

        $scope.obtenerFletesRealizados = function () {
            var idUser = {"usuarioId": session.get('usuario').id, "fechaDesde": $scope.fechaDesde, "fechaHasta": $scope.fechaHasta};
            api.post('flete/obtenerListaFletesRealizados', idUser, $scope.cargarListaFletesRealizados);

        };

        $scope.cargarListaFletesRealizados = function (resp) {
            $scope.listadoRealizados = resp.data;
        };

        $scope.buscarFletes = function () {
            $scope.obtenerFletesPendientes();
            $scope.obtenerFletesRealizados();
        };
        
        $scope.obtenerFletesPendientes();
        $scope.obtenerFletesRealizados();
    }
]);