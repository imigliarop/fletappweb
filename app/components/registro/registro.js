fletapp.controller('registroCtrl', [
    '$scope',
    'Api',
    '$routeParams',
    function(scope, Api, $routeParams)
  {      
    scope.tipoUsuario = $routeParams.tipoUsuario;
    scope.usuario = {
            email 	: 	"",
            nombre 	: 	"",
            apellido 	: 	"",
            telefono	: 	"",
            password 	: 	"",
            tipoUsuario :       scope.tipoUsuario
        };
    scope.registro = function ()
    {        
        Api.post("auth/registrar", scope.usuario, success);
    };
    
    var success = function(response){
        scope.mensaje = response.data.mensaje;
    };   
}]);
 
fletapp.directive('passCheck', [function () {
    return {
        require: 'ngModel',
        link: function (scope, elem, attrs, ctrl) {
            var firstPassword = '#' + attrs.passCheck;
            elem.add(firstPassword).on('keyup', function () {
                scope.$apply(function () {
                    var v = elem.val()===$(firstPassword).val();
                    ctrl.$setValidity('passmatch', v);
                });
            });
        }
    };
}]);
/*fletapp.controller('registroCtrl', [
    '$scope',
    'Api',
    function(scope, Api)
  {      
    scope.tipoUsuario = "Cliente"; //Comentar cuando se implemente el redirect desde home  
    scope.usuario = {
            email 	: 	"",
            nombre 	: 	"",
            apellido 	: 	"",
            telefono	: 	"",
            password 	: 	"",
            tipoUsuario :       scope.tipoUsuario
        };
    scope.registro = function ()
    {        
        Api.post("usuario/registrar", scope.usuario, success);
    };
    
    var success = function(response){
        scope.mensaje = response;
    };   
}]);
 
fletapp.directive('passCheck', [function () {
    return {
        require: 'ngModel',
        link: function (scope, elem, attrs, ctrl) {
            var firstPassword = '#' + attrs.passCheck;
            elem.add(firstPassword).on('keyup', function () {
                scope.$apply(function () {
                    var v = elem.val()===$(firstPassword).val();
                    ctrl.$setValidity('passmatch', v);
                });
            });
        }
    };
}]);
*/