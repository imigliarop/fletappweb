fletapp.controller('fleteroHomeCtrl', ['$rootScope', '$scope', 'Session', '$location', 'Api', '$timeout',
    function ($rootScope, $scope, session, $location, api, $timeout) {

        var usuarioId = session.get("usuario").id;

        var cargarVehiculo = function (vehiculo) {
            if (vehiculo.data === "") {
                $scope.vehiculo = null;
            } else {
                $scope.vehiculo = vehiculo.data;
            }

            if ($scope.vehiculo === null) {
                $scope.botonVehiculo = "Crear";
                $('#agregarVehiculoModal').modal('show');
            } else {
                $scope.botonVehiculo = "Actualizar";
            }
            $scope.vehiculoForm.$setPristine();
        };
        
        var cargarFletes = function (fleteRes){
            $scope.listadoFletes = fleteRes.data;
        };

        //Hacer el get del vehiculo
        api.get('usuario/vehiculoFletero', {"fleteroId": usuarioId}, cargarVehiculo);
        
        //Hacer el get de los proximos fletes
        api.get('flete/proximosFletes', {"fleteroId": usuarioId}, cargarFletes);


        $scope.opcionesMapa = {
            tipo: 'consultaProximoFlete'
        };

        $scope.seleccionarFlete = function (flete) {
            $scope.fleteSeleccionado = flete;
            $scope.opcionesMapa.ptoInicio = flete.puntoOrigen;
            $scope.opcionesMapa.ptoFin = flete.puntoDestino;
        };

        $scope.guardarVehiculo = function () {
            //Validar el formulario
            if ($scope.vehiculoForm.$invalid) {
                $scope.vehiculoForm.$dirty = true;
                return;
            }

            if ($scope.vehiculo.id == null) {
                $scope.mensaje = "El vehículo fue creado correctamente";
            } else {
                $scope.mensaje = "Se actualizaron los datos del vehículo";
            }

            api.post('usuario/vehiculoFletero', $scope.vehiculo, vehiculoActualizado, {"fleteroId": usuarioId});
        };

        var vehiculoActualizado = function (res) {
            $('#successModal').modal('show');
            $timeout(function () {
                cargarVehiculo(res);
                $('#successModal').modal('hide');
            }, 2500);
        };

    }
]);