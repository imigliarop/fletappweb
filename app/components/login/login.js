fletapp.controller('loginCtrl', [
    '$scope',
    '$rootScope',
    'Api',
    'Session',
    '$location',
    '$routeParams',
    function(scope, rootScope, Api, Session, location, $routeParams) {
        scope.tipoUsuario = $routeParams.tipoUsuario;
        
        scope.usuario = {
            email 	: 	"",
            password 	: 	"",
            tipo        :       scope.tipoUsuario
        };
        
        scope.login = function() { 
            var email = window.encodeURIComponent(scope.usuario.email);
            var password = window.encodeURIComponent(scope.usuario.password);
            var tipo = window.encodeURIComponent(scope.usuario.tipo);
            var usuarioUrl = "email="+email+"&"+"password="+password+"&"+"tipo="+tipo;
            Api.login(usuarioUrl, callback);         
        };

        function callback(response) {
            rootScope.user = response.data;
            Session.set('usuario', response.data);
            var us = Session.get('usuario');
            console.log(us);
            location.path("/home");
        };
  }
]);