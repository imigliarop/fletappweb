fletapp.controller('homeCtrl', ['$location', 'Session',
    function($location, session){
        console.log("homeCtrl");
        
        if (session.get("usuario")) {
            var usu = session.get("usuario");
            if (usu.tipo === "cliente") {
                $location.path('/clienteHome');
            } else {
                $location.path('/fleteroHome');
            }
        }
    }
]);

