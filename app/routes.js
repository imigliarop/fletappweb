fletapp.config(function($routeProvider) {

    $routeProvider.when("/home", {
        templateUrl : 'app/components/home/home.html',
        controller : 'homeCtrl'
    });

    $routeProvider.when("/openLayer", {
        templateUrl : 'app/components/openLayer/openLayer.html',
        controller : 'openLayerCtrl'
    });

    $routeProvider.when("/registro/:tipoUsuario", {
        templateUrl : 'app/components/registro/registro.html',
        controller : 'registroCtrl'
    });
    
    $routeProvider.when("/login/:tipoUsuario", {
        templateUrl : 'app/components/login/login.html',
        controller : 'loginCtrl'
    });
    
    $routeProvider.when("/clienteHome", {
        templateUrl : 'app/components/clienteHome/clienteHome.html',
        controller : 'clienteHomeCtrl'
    });
    
    $routeProvider.when("/clienteHistorial", {
        templateUrl : 'app/components/clienteHistorial/clienteHistorial.html',
        controller : 'clienteHistorialCtrl'
    });
    
    $routeProvider.when("/fleteroHome", {
        templateUrl : 'app/components/fleteroHome/fleteroHome.html',
        controller : 'fleteroHomeCtrl'
    });
    
    $routeProvider.when("/zonasDeTrabajo", {
        templateUrl : 'app/components/zonasDeTrabajo/zonasDeTrabajo.html',
        controller : 'zonasDeTrabajoCtrl'
    });
    
    $routeProvider.when("/solicitudesPendientes", {
    templateUrl : 'app/components/solicitudesPendientes/solicitudesPendientes.html',
    controller : 'solicitudesPendientesCtrl'
    });
    
    $routeProvider.otherwise({
        redirectTo: '/home'
    });
});
