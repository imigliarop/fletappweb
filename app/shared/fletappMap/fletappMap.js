/* global ol */

//Declaro la directiva fletappMap (que en html seria fletapp-map) del modulo fletapp
fletapp.directive('fletappMap', function () {
    return{
        //Le decimos que es del tipo Element, o sea se usaria asi en el html:
        //<fletapp-map></fletapp-map>
        restrict: 'E',
        //Le decimos que pagina usar como template para la vista
        templateUrl: "app/shared/fletappMap/fletappMap.html",
        //Le decimos que parametros va a recibir en el scope esta directiva
        //<fletapp-map opciones="{}" geo-objeto="{}"></fletapp-map>
        scope: {
            //El atributo tipo se pasa por valor
            opciones: "=",
            //El atributo geoObjeto se pasa por referencia para que se vean los cambios en el controlador que llama a la directiva
            geoObjeto: "="
        },
        //Esta seccion se carga previa al controlador, es util por ejemplo para observar objetos
        link: {
            pre: function (scope) {
                //Por ejemplo si options cambia, podemos llamar algun metodo del controller 
                scope.$watch('opciones', function () {
                    switch (scope.opciones.tipo) {
                        case 'solicitudFlete':
                            scope.cargarFlete();
                            break;
                        case 'zonasDeTrabajo':
                            scope.cargarZona();
                            break;
                        case 'historialFlete':
                            scope.cargarFlete();
                            break;
                        case 'consultaProximoFlete':
                            scope.cargarFlete();
                            break;
                        default:
                            break;
                    }
                }, true);
            }
        },
        //Controller que maneja la logica de la directiva
        controller: ['$scope', function ($scope) {

                $scope.cargarFlete = function () {
                    if ($scope.layerInicio !== null) {
                        map.removeLayer($scope.layerInicio);
                    }
                    if ($scope.layerFin !== null) {
                        map.removeLayer($scope.layerFin);
                    }

                    if ($scope.opciones.fleteSeleccionado !== null) {
                        //LAYER DE PUNTO DE INICIO
                        var vectorLayerInicio = new ol.layer.Vector({
                            source: new ol.source.Vector({
                                format: new ol.format.GeoJSON(),
                                url: 'http://localhost:8081/geoserver/TSIG/ows?service=WFS&version=1.1.0' +
                                        '&request=GetFeature&typeName=TSIG:fletes&featureID='
                                        + $scope.opciones.ptoInicio +
                                        '&srsname=EPSG:32721&outputFormat=application%2Fjson'
                            }),
                            style: new ol.style.Style({
                                image: new ol.style.Circle({
                                    radius: 7,
                                    fill: new ol.style.Fill({
                                        color: '#42f4b0'
                                    })
                                })
                            })
                        });
                        map.addLayer(vectorLayerInicio);

                        // LAYER DE PUNTO FINAL
                        $scope.layerInicio = vectorLayerInicio;
                        var vectorLayerFin = new ol.layer.Vector({
                            source: new ol.source.Vector({
                                format: new ol.format.GeoJSON(),
                                url: 'http://localhost:8081/geoserver/TSIG/ows?service=WFS&version=1.1.0' +
                                        '&request=GetFeature&typeName=TSIG:fletes&featureID='
                                        + $scope.opciones.ptoFin +
                                        '&srsname=EPSG:32721&outputFormat=application%2Fjson'
                            }),
                            style: new ol.style.Style({
                                image: new ol.style.Circle({
                                    radius: 7,
                                    fill: new ol.style.Fill({
                                        color: '#e87e40'
                                    })
                                })
                            })
                        });
                        map.addLayer(vectorLayerFin);
                        $scope.layerFin = vectorLayerFin;

                        // FALTA CARGAR UN ARRAY CON EL PUNTO MEDIO ENTRE PUNTOS
                        var view = new ol.View({
                            center: [578471.5167414253, 6137787.661694823],
                            projection: projection,
                            zoom: 13,
                            minZoom: 11,
                            maxZoom: 16
                        });
                        map.setView(view);
                    }
                };

                $scope.cargarZona = function () {
                    if ($scope.layerZona !== null) {
                        map.removeLayer($scope.layerZona);
                    }

                    if ($scope.opciones.zonaSeleccionada !== null) {
                        var vectorLayer = new ol.layer.Vector({
                            source: new ol.source.Vector({
                                format: new ol.format.GeoJSON(),
                                url: 'http://localhost:8081/geoserver/TSIG/ows?service=WFS&version=1.1.0' +
                                        '&request=GetFeature&typeName=TSIG:zonas&featureID='
                                        + $scope.opciones.zonaSeleccionada +
                                        '&srsname=EPSG:32721&outputFormat=application%2Fjson'
                            }),
                            style: new ol.style.Style({
                                fill: new ol.style.Fill({
                                    color: 'rgba(66, 244, 176, 0.3)'
                                }),
                                stroke: new ol.style.Stroke({
                                    color: '#42f4b0',
                                    width: 2
                                }),
                                image: new ol.style.Circle({
                                    radius: 7,
                                    fill: new ol.style.Fill({
                                        color: '#42f4b0'
                                    })
                                })
                            })
                        });
                        map.addLayer(vectorLayer);
                        $scope.layerZona = vectorLayer;
                        // FALTA CARGAR UN ARRAY CON EL PUNTO MEDIO ENTRE PUNTOS
                        var view = new ol.View({
                            center: [578471.5167414253, 6137787.661694823],
                            projection: projection,
                            zoom: 13,
                            minZoom: 11,
                            maxZoom: 16
                        });
                        map.setView(view);
                        if ($scope.drawZona !== null) {
                            map.removeInteraction($scope.drawZona);
                        }
                    }
                };
                //Tipo va a ser:
                //Para el cliente: solicitudFlete, consultaFlete
                //Para el fletero: consultaProximoFlete, zonasDeTrabajo, solicitudesPendientes
                //GENERAMOS EL MAPA INICIAL PARA TODAS LAS PANTALLAS.

                var wmsSource = new ol.source.ImageWMS({
                    url: 'http://localhost:8081/geoserver/TSIG/wms',
                    params: {'layers': 'TSIG:TSIG', 'bbox': '551796.375,6133367.0,589413.5,6159942.0'},
                    serverType: 'geoserver',
                    crossOrigin: 'anonymous'
                });

                var wmsLayer = new ol.layer.Image({
                    source: wmsSource
                });

                var projection = new ol.proj.Projection({
                    code: 'EPSG:32721',
                    units: 'm'
                });

                var map = new ol.Map({
                    layers: [wmsLayer],
                    target: 'map',
                    view: new ol.View({
                        center: [578471.5167414253, 6137787.661694823],
                        projection: projection,
                        zoom: 13,
                        minZoom: 11,
                        maxZoom: 16
                    })
                });

                var formatWFS = new ol.format.WFS();

                var xs = new XMLSerializer();
                var formatGMLFlete = new ol.format.GML({
                    featureNS: 'TSIG',
                    featureType: 'fletes'
                });
                var formatGMLZona = new ol.format.GML({
                    featureNS: 'TSIG',
                    featureType: 'zonas'
                });
                var features = new ol.Collection();
                var featureOverlay = new ol.layer.Vector({
                    source: new ol.source.Vector({features: features}),
                    style: new ol.style.Style({
                        fill: new ol.style.Fill({
                            color: 'rgba(66, 244, 176, 0.3)'
                        }),
                        stroke: new ol.style.Stroke({
                            color: '#42f4b0',
                            width: 2
                        }),
                        image: new ol.style.Circle({
                            radius: 7,
                            fill: new ol.style.Fill({
                                color: '#42f4b0'
                            })
                        })
                    })
                });
                featureOverlay.setMap(map);
                var featuresAux = new ol.Collection();
                var featureOverlayAux = new ol.layer.Vector({
                    source: new ol.source.Vector({features: featuresAux}),
                    style: new ol.style.Style({
                        image: new ol.style.Circle({
                            radius: 7,
                            fill: new ol.style.Fill({
                                color: '#e87e40'
                            })
                        })
                    })
                });
                featureOverlayAux.setMap(map);
                $scope.ptoInicio = false;
                $scope.funcionInicio = function () {
                    draw = new ol.interaction.Draw({
                        features: features,
                        type: 'Point',
                        style: new ol.style.Style({
                            image: new ol.style.Circle({
                                radius: 7,
                                fill: new ol.style.Fill({
                                    color: '#42f4b0'
                                })
                            })
                        })
                    });
                    map.addInteraction(draw);
                    draw.on('drawend', function (evt) {
                        var featureInicio = evt.feature;
                        var nodoInicio = formatWFS.writeTransaction([featureInicio], null, null, formatGMLFlete);
                        if ($scope.geoObjeto.find(g => g.punto === 'origen')) {
                            $scope.geoObjeto.find(g => g.punto === 'origen').payload = xs.serializeToString(nodoInicio);
                        } else {
                            $scope.geoObjeto.push({punto: 'origen', payload: xs.serializeToString(nodoInicio)});
                        }
                        map.removeInteraction(draw);
                        $scope.$apply();
                    });
                };

                $scope.funcionFin = function () {
                    draw = new ol.interaction.Draw({
                        features: featuresAux,
                        type: 'Point',
                        style: new ol.style.Style({
                            image: new ol.style.Circle({
                                radius: 7,
                                fill: new ol.style.Fill({
                                    color: '#e87e40'
                                })
                            })
                        })
                    });
                    map.addInteraction(draw);
                    draw.on('drawend', function (evt) {
                        var featureFin = evt.feature;
                        var nodoFin = formatWFS.writeTransaction([featureFin], null, null, formatGMLFlete);
                        if ($scope.geoObjeto.find(g => g.punto === 'destino')) {
                            $scope.geoObjeto.find(g => g.punto === 'destino').payload = xs.serializeToString(nodoFin);
                        } else {
                            $scope.geoObjeto.push({punto: 'destino', payload: xs.serializeToString(nodoFin)});
                        }
                        map.removeInteraction(draw);
                        $scope.$apply();
                    });
                };

                $scope.funcionZona = function () {
                    draw = new ol.interaction.Draw({
                        features: features,
                        type: 'Polygon',
                        style: new ol.style.Style({
                            fill: new ol.style.Fill({
                                color: 'rgba(66, 244, 176, 0.3)'
                            }),
                            stroke: new ol.style.Stroke({
                                color: '#42f4b0',
                                width: 2
                            }),
                            image: new ol.style.Circle({
                                radius: 7,
                                fill: new ol.style.Fill({
                                    color: '#42f4b0'
                                })
                            })
                        })
                    });
                    map.addInteraction(draw);
                    $scope.drawZona = draw;
                    draw.on('drawend', function (evt) {
                        var featureZona = evt.feature;
                        var nodoZona = formatWFS.writeTransaction([featureZona], null, null, formatGMLZona);
                        $scope.geoObjeto = xs.serializeToString(nodoZona);
                        map.removeInteraction(draw);
                        $scope.$apply();
                    });
                };
                // MODIFICANDO LO DIBUJADO!
                var modify = new ol.interaction.Modify({
                    features: features
                });
                map.addInteraction(modify);

                //Inicializa los objetos
                switch ($scope.opciones.tipo) {
                    case 'solicitudFlete':
                        $scope.geoObjeto = [];
                        break;
                    case 'zonasDeTrabajo':
                        $scope.geoObjeto = null;
                        break;
                    case 'historialFlete':

                        break;
                    case 'solicitudesPendientes':
                        break;
                    default:
                        $scope.geoObjeto = null;
                        break;
                }
            }]
    };
});
