fletapp.controller('headerCtrl', ['$scope', '$rootScope', '$location', 'Session', function ($scope, $rootScope, $location, session)
    {
        $rootScope.user = session.get("usuario");

        $rootScope.$watch('user', function () {
            if ($rootScope.user !== null) {
                if ($rootScope.user.tipo === "cliente") {
                    $scope.homeLink = "#/homeCliente";
                } else {
                    $scope.homeLink = "#/homeFletero";
                }
            } else {
                $scope.homeLink = "#/home";
            }
        });
        
        $scope.salir = function(){
            session.delete('usuario');
            $rootScope.user = null;
            $location.path('/home');
        };
    }
]);