fletapp.service('Api', [
    'global',
    '$http',
    function (global, http) {
        this.post = function(action, data, callback, parametros){            
            http.post(global.serverUrl + action, data, {
                headers: {
                    'Content-Type': 'application/json'
                },
                params: parametros
            })
            .then(function (response) {
                callback(response);
            },function (error) {
                console.log("HORROOOOOOOOR", error);
            });
        };
        
        this.login = function(data, callback){            
            http.post(global.serverUrl + "auth/login", data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            })
            .then(function (response) {
                callback(response);
            },function (error) {
                console.log("error", error);
            });
        };
        
        this.get = function(action, data, callback){
            http.get(global.serverUrl + action, {
                headers: {
                    'Content-Type': 'application/json'
                },
                params: data
            }).then(function(resp){
                callback(resp);
            },function (error) {
                console.log("error", error);
            });
        };
    }
]);
