fletapp.service('geoService', ['global', '$http',
    function (global, $http) {

        //Para guardar las geometrias
        this.geoPost = function (payload) {

            var config = {
                service: 'WFS',
                processData: false,
                dataType: 'xml',
                contentType: 'text/xml'
            };

            return $http.post(global.geoServer + "wfs", payload, config);
        };

        this.formatXml = function (xml) {
            var formatWFS = new ol.format.WFS();
            var result = formatWFS.readTransactionResponse(xml);
            return result.insertIds[0];
        };
        
        this.getZonasParaSolicitud = function (puntoOrigen, puntoDestino){
            var idOrigen = obtenerSoloNumero(puntoOrigen);
            var idDestino = obtenerSoloNumero(puntoDestino);
            var url = global.geoServer + "TSIG/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=TSIG:" +
                                            "zonasPosibles&maxFeatures=50&outputFormat=application%2Fjson&viewparams=" +
                                            "ptoInicio:" + idOrigen + ";ptoFin:" + idDestino;
            
            return $http.get(url);
        };
        
        this.getDistancia = function (puntoOrigen, puntoDestino){
            var idOrigen = obtenerSoloNumero(puntoOrigen);
            var idDestino = obtenerSoloNumero(puntoDestino);
            
            var url = global.geoServer + "TSIG/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=TSIG:" +
                                            "distancia&maxFeatures=50&outputFormat=application%2Fjson&viewparams=" +
                                            "inicio:" +idOrigen + ";destino:" + idDestino;
            return $http.get(url);
        };
        
        var obtenerSoloNumero = function(string){
            var numero = string.match(/\d+/);
            return numero[0];
        };
        
        this.getDistanciaDeSolicitud = function(idZona, puntoOrigen, puntoDestino){
            var idOrigen  = obtenerSoloNumero(puntoOrigen);
            var idDestino = obtenerSoloNumero(puntoDestino);
            var idZonaNum = obtenerSoloNumero(idZona);
            var url = global.geoServer + "TSIG/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=TSIG:" +
                                         "fleteEnZona&maxFeatures=50&outputFormat=application%2Fjson&viewparams=" +
                                         "zona:"+idZonaNum+ ";inicio:" + idOrigen + ";fin:" + idDestino; 
            return $http.get(url);                     
        };
    }
]);
