fletapp.service('Session', [
    function () {
        this.set = function (key, value) {
            return localStorage.setItem(key, JSON.stringify(value));
        };
        this.get = function (key) {
            var item = localStorage.getItem(key),
                $response = typeof item !== 'undefined' && item !== 'undefined' ? JSON.parse(item) : null;
            return $response;
        },
        this.delete = function (key) {
            return localStorage.removeItem(key);
        };
    }
]);


        