var fletapp = angular.module('fletapp', [ 'ngRoute', 'openlayers-directive', 'ui.bootstrap.datetimepicker', 'ui.dateTimeInput', 'jkAngularRatingStars' ]);

fletapp.config(['$locationProvider', function($locationProvider) {
  $locationProvider.hashPrefix('');
}]);